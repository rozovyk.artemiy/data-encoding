open Binary_error_types

let raise e = raise (Read_error e)

type field = {name : string; value : bytes}

type state = {
  buffer : bytes;
  mutable offset : int;
  mutable remaining_bytes : int;
  mutable allowed_bytes : int option;
  mutable fields : field list;
}

let check_allowed_bytes state size =
  match state.allowed_bytes with
  | Some len when len < size ->
      raise Size_limit_exceeded
  | Some len ->
      Some (len - size)
  | None ->
      None

let check_remaining_bytes state size =
  if state.remaining_bytes < size then raise Not_enough_data ;
  state.remaining_bytes - size

let read_atom size conv name state =
  let offset = state.offset in
  state.remaining_bytes <- check_remaining_bytes state size ;
  state.allowed_bytes <- check_allowed_bytes state size ;
  state.offset <- state.offset + size ;
  state.fields <-
    {name; value = Bytes.sub state.buffer offset size} :: state.fields ;
  conv state.buffer offset

(** Reader for all the atomic types. *)
module Atom = struct
  let uint8 = read_atom Binary_size.uint8 TzEndian.get_uint8

  let uint16 = read_atom Binary_size.int16 TzEndian.get_uint16

  let int8 = read_atom Binary_size.int8 TzEndian.get_int8

  let int16 = read_atom Binary_size.int16 TzEndian.get_int16

  let int32 = read_atom Binary_size.int32 TzEndian.get_int32

  let int64 = read_atom Binary_size.int64 TzEndian.get_int64

  let float = read_atom Binary_size.float TzEndian.get_double

  let bool state name = int8 state name <> 0

  let uint30 =
    read_atom Binary_size.uint30
    @@ fun buffer ofs ->
    let v = Int32.to_int (TzEndian.get_int32 buffer ofs) in
    if v < 0 then raise (Invalid_int {min = 0; v; max = (1 lsl 30) - 1}) ;
    v

  let int31 =
    read_atom Binary_size.int31
    @@ fun buffer ofs -> Int32.to_int (TzEndian.get_int32 buffer ofs)

  let int = function
    | `Int31 ->
        int31
    | `Int16 ->
        int16
    | `Int8 ->
        int8
    | `Uint30 ->
        uint30
    | `Uint16 ->
        uint16
    | `Uint8 ->
        uint8

  let ranged_int ~minimum ~maximum state name =
    let read_int =
      match Binary_size.range_to_size ~minimum ~maximum with
      | `Int8 ->
          int8
      | `Int16 ->
          int16
      | `Int31 ->
          int31
      | `Uint8 ->
          uint8
      | `Uint16 ->
          uint16
      | `Uint30 ->
          uint30
    in
    let ranged = read_int state name in
    let ranged = if minimum > 0 then ranged + minimum else ranged in
    if not (minimum <= ranged && ranged <= maximum) then
      raise (Invalid_int {min = minimum; v = ranged; max = maximum}) ;
    ranged

  let ranged_float ~minimum ~maximum state name =
    let ranged = float state name in
    if not (minimum <= ranged && ranged <= maximum) then
      raise (Invalid_float {min = minimum; v = ranged; max = maximum}) ;
    ranged

  let rec read_z res value bit_in_value state name =
    let byte = uint8 state name in
    let value = value lor ((byte land 0x7F) lsl bit_in_value) in
    let bit_in_value = bit_in_value + 7 in
    let (bit_in_value, value) =
      if bit_in_value < 8 then (bit_in_value, value)
      else (
        Buffer.add_char res (Char.unsafe_chr (value land 0xFF)) ;
        (bit_in_value - 8, value lsr 8) )
    in
    if byte land 0x80 = 0x80 then read_z res value bit_in_value state name
    else (
      if bit_in_value > 0 then Buffer.add_char res (Char.unsafe_chr value) ;
      if byte = 0x00 then raise Trailing_zero ;
      Z.of_bits (Buffer.contents res) )

  let n state name =
    let first = uint8 state name in
    let first_value = first land 0x7F in
    if first land 0x80 = 0x80 then
      read_z (Buffer.create 100) first_value 7 state name
    else Z.of_int first_value

  let z state name =
    let first = uint8 state name in
    let first_value = first land 0x3F in
    let sign = first land 0x40 <> 0 in
    if first land 0x80 = 0x80 then
      let n = read_z (Buffer.create 100) first_value 6 state name in
      if sign then Z.neg n else n
    else
      let n = Z.of_int first_value in
      if sign then Z.neg n else n

  let string_enum arr state name =
    let read_index =
      match Binary_size.enum_size arr with
      | `Uint8 ->
          uint8
      | `Uint16 ->
          uint16
      | `Uint30 ->
          uint30
    in
    let index = read_index state name in
    if index >= Array.length arr then raise No_case_matched ;
    arr.(index)

  let fixed_length_bytes length =
    read_atom length @@ fun buf ofs -> Bytes.sub buf ofs length

  let fixed_length_string length =
    read_atom length @@ fun buf ofs -> Bytes.sub_string buf ofs length

  let tag = function `Uint8 -> uint8 | `Uint16 -> uint16
end

(** Main recursive reading function, in continuation passing style. *)
let rec read_rec : type ret. ret Encoding.t -> string -> state -> ret =
 fun e p state ->
  let ( !! ) x = if p = "" then x else x ^ " " ^ " (" ^ p ^ ")" in
  let open Encoding in
  match e.encoding with
  | Null ->
      ()
  | Empty ->
      ()
  | Constant _ ->
      ()
  | Ignore ->
      ()
  | Bool ->
      Atom.bool !!"bool" state
  | Int8 ->
      Atom.int8 !!"int8" state
  | Uint8 ->
      Atom.uint8 !!"uint8" state
  | Int16 ->
      Atom.int16 !!"int16" state
  | Uint16 ->
      Atom.uint16 !!"uint16" state
  | Int31 ->
      Atom.int31 !!"int31" state
  | Int32 ->
      Atom.int32 !!"int32" state
  | Int64 ->
      Atom.int64 !!"int64" state
  | N ->
      Atom.n !!"N" state
  | Z ->
      Atom.z !!"Z" state
  | Float ->
      Atom.float !!"float" state
  | Bytes (`Fixed n) ->
      Atom.fixed_length_bytes n !!"bytes" state
  | Bytes `Variable ->
      Atom.fixed_length_bytes state.remaining_bytes !!"bytes" state
  | String (`Fixed n) ->
      Atom.fixed_length_string n !!"string" state
  | String `Variable ->
      Atom.fixed_length_string state.remaining_bytes !!"string" state
  | Padded (e, n) ->
      let v = read_rec e p state in
      ignore (Atom.fixed_length_string n "padding" state : string) ;
      v
  | RangedInt {minimum; maximum} ->
      Atom.ranged_int ~minimum ~maximum !!"ranged int" state
  | RangedFloat {minimum; maximum} ->
      Atom.ranged_float ~minimum ~maximum !!"ranged float" state
  | String_enum (_, arr) ->
      Atom.string_enum arr !!"enum" state
  | Array (max_length, e) ->
      let max_length = match max_length with Some l -> l | None -> max_int in
      let l = read_list List_too_long max_length e p state in
      Array.of_list l
  | List (max_length, e) ->
      let max_length = match max_length with Some l -> l | None -> max_int in
      read_list Array_too_long max_length e p state
  | Obj (Req {encoding = e; name; _}) ->
      read_rec e name state
  | Obj (Dft {encoding = e; name; _}) ->
      read_rec e name state
  | Obj (Opt {kind = `Dynamic; encoding = e; name; _}) ->
      let present = Atom.bool (name ^ " presence flag") state in
      if not present then None else Some (read_rec e !!name state)
  | Obj (Opt {kind = `Variable; encoding = e; name; _}) ->
      if state.remaining_bytes = 0 then None
      else Some (read_rec e !!name state)
  | Objs {kind = `Fixed sz; left; right} ->
      ignore (check_remaining_bytes state sz : int) ;
      ignore (check_allowed_bytes state sz : int option) ;
      let left = read_rec left p state in
      let right = read_rec right p state in
      (left, right)
  | Objs {kind = `Dynamic; left; right} ->
      let left = read_rec left p state in
      let right = read_rec right p state in
      (left, right)
  | Objs {kind = `Variable; left; right} ->
      read_variable_pair left right p state
  | Tup e ->
      read_rec e p state
  | Tups {kind = `Fixed sz; left; right} ->
      ignore (check_remaining_bytes state sz : int) ;
      ignore (check_allowed_bytes state sz : int option) ;
      let left = read_rec left p state in
      let right = read_rec right p state in
      (left, right)
  | Tups {kind = `Dynamic; left; right} ->
      let left = read_rec left p state in
      let right = read_rec right p state in
      (left, right)
  | Tups {kind = `Variable; left; right} ->
      read_variable_pair left right p state
  | Conv {inj; encoding; _} ->
      inj (read_rec encoding p state)
  | Union {tag_size; cases; _} ->
      let ctag = Atom.tag tag_size "DUMMY" state in
      let (Case {encoding; inj; _}) =
        try
          List.find
            (function
              | Case {tag = tg; title; _} ->
                
                  if Uint_option.is_some tg && Uint_option.get tg = ctag then (
                    let {value; _} = List.hd state.fields in
                    state.fields <-
                      {name = title ^ " tag"; value} :: List.tl state.fields ;
                    true )
                  else false
              )
            cases
        with Not_found -> raise (Unexpected_tag ctag)
      in
      inj (read_rec encoding p state)
  | Dynamic_size {kind; encoding = e} ->
      let sz = Atom.int kind "dynamic length" state in
      let remaining = check_remaining_bytes state sz in
      state.remaining_bytes <- sz ;
      ignore (check_allowed_bytes state sz : int option) ;
      let v = read_rec e p state in
      if state.remaining_bytes <> 0 then raise Extra_bytes ;
      state.remaining_bytes <- remaining ;
      v
  | Check_size {limit; encoding = e} ->
      let old_allowed_bytes = state.allowed_bytes in
      let limit =
        match state.allowed_bytes with
        | None ->
            limit
        | Some current_limit ->
            min current_limit limit
      in
      state.allowed_bytes <- Some limit ;
      let v = read_rec e p state in
      let allowed_bytes =
        match old_allowed_bytes with
        | None ->
            None
        | Some old_limit ->
            let remaining =
              match state.allowed_bytes with
              | None ->
                  assert false
              | Some remaining ->
                  remaining
            in
            let read = limit - remaining in
            Some (old_limit - read)
      in
      state.allowed_bytes <- allowed_bytes ;
      v
  | Describe {encoding = e; id; _} ->
      read_rec e !!id state
  | Splitted {encoding = e; _} ->
      read_rec e p state
  | Mu {fix; name; _} ->
      read_rec (fix e) !!name state
  | Delayed f ->
      read_rec (f ()) p state

and read_variable_pair :
    type left right.
    left Encoding.t -> right Encoding.t -> string -> state -> left * right =
 fun e1 e2 p state ->
  match (Encoding.classify e1, Encoding.classify e2) with
  | ((`Dynamic | `Fixed _), `Variable) ->
      let left = read_rec e1 p state in
      let right = read_rec e2 p state in
      (left, right)
  | (`Variable, `Fixed n) ->
      if n > state.remaining_bytes then raise Not_enough_data ;
      state.remaining_bytes <- state.remaining_bytes - n ;
      let left = read_rec e1 p state in
      assert (state.remaining_bytes = 0) ;
      state.remaining_bytes <- n ;
      let right = read_rec e2 p state in
      assert (state.remaining_bytes = 0) ;
      (left, right)
  | _ ->
      assert false

(* Should be rejected by [Encoding.Kind.combine] *)
and read_list :
    type a. read_error -> int -> a Encoding.t -> string -> state -> a list =
 fun error max_length e p state ->
  let rec loop max_length acc =
    if state.remaining_bytes = 0 then List.rev acc
    else if max_length = 0 then raise error
    else
      let v = read_rec e (p ^ " element") state in
      loop (max_length - 1) (v :: acc)
  in
  loop max_length []

(** ******************** *)

(** Various entry points *)

let decoupe encoding buffer ofs len =
  let state =
    {
      buffer;
      offset = ofs;
      fields = [];
      remaining_bytes = len;
      allowed_bytes = None;
    }
  in
  match read_rec encoding "" state with
  | exception Read_error _ ->
      None
  | _ ->
      Some (List.rev state.fields)

let decoupe_bytes_exn encoding buffer =
  let len = Bytes.length buffer in
  let state =
    {
      buffer;
      offset = 0;
      fields = [];
      remaining_bytes = len;
      allowed_bytes = None;
    }
  in
  let _ = read_rec encoding "" state in
  if state.offset <> len then raise Extra_bytes ;
  List.rev state.fields

let decoupe_bytes encoding buffer =
  try Some (decoupe_bytes_exn encoding buffer) with Read_error _ -> None
