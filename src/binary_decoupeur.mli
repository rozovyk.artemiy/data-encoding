type field = {name : string; value : bytes}

val decoupe : _ Encoding.t -> bytes -> int -> int -> field list option

val decoupe_bytes : _ Encoding.t -> bytes -> field list option

val decoupe_bytes_exn : _ Encoding.t -> bytes -> field list
